---
home: true
heroImage: /akf-logo.png
tagline:
actionText: See Principles →
actionLink: /guide/
footer: Site made by Eric Arrington at AKF Partners using VuePress
---
