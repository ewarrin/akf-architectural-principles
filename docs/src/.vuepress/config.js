const { description } = require("../../package");

module.exports = {
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#title
   */
  title: "AKF Architectural Principles",
  /**
   * Ref：https://v1.vuepress.vuejs.org/config/#description
   */
  description: description,

  /**
   * Extra tags to be injected to the page HTML `<head>`
   *
   * ref：https://v1.vuepress.vuejs.org/config/#head
   */
  head: [
    ["meta", { name: "theme-color", content: "#38bdf8" }],
    ["meta", { name: "apple-mobile-web-app-capable", content: "yes" }],
    [
      "meta",
      { name: "apple-mobile-web-app-status-bar-style", content: "black" },
    ],
  ],

  /**
   * Theme configuration, here is the default theme configuration for VuePress.
   *
   * ref：https://v1.vuepress.vuejs.org/theme/default-theme-config.html
   */
  themeConfig: {
    repo: "",
    editLinks: false,
    docsDir: "",
    editLinkText: "",
    lastUpdated: false,
    nav: [
      {
        text: "Guide",
        link: "/guide/",
      },
      {
        text: "AKF Partners Website",
        link: "https://akfpartners.com/",
      },
      {
        text: "Repo",
        link: "https://gitlab.com/ewarrin/akf-architectural-principles",
      },
    ],
    sidebar: {
      "/guide/": [
        {
          title: "Overview",
          collapsable: false,
          children: ["overview", "risk-model", "the-akf-architect"],
        },
        {
          title: "Architectural Principles",
          collapsable: false,
          children: [
            "mature-technology",
            "build-vs-buy",
            "adopt-technology-thoughtfully",
            "use-common-language-and-definitions",
            "architect-for-the-user",
            "portability",
            "design-to-scale",
            "plan-for-failure",
            "make-it-manageable",
            "maintainability",
            "design-for-security-privacy-compliance",
            "scale-out-not-up",
            "isolate-faults",
            "design-for-at-least-two-axes",
            "n-plus-2-design",
            "design-to-be-disabled",
            "microservices-for-depth",
            "build-small-release-small-fail-fast",
            "automation-over-people",
            "stateless-systems",
            "design-to-be-monitored",
            "design-for-rollback"
          ],
        },
      ],
    },
  },

  /**
   * Apply plugins，ref：https://v1.vuepress.vuejs.org/zh/plugin/
   */
  plugins: ["@vuepress/plugin-back-to-top", "@vuepress/plugin-medium-zoom"],
};
