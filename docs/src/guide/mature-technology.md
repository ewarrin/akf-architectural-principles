# Use Mature Technology

When choosing to buy a solution (software, hardware, etc.) that is not part of your competitive advantage, choose a proven product

Becoming an early adopter of software or systems to be on the cutting edge also means being on the leading edge of finding all the bugs with that software or system

