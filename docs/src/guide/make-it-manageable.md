# Make it Manageable

## Make it manageable and plan for quality of service

- Design to be monitored - logging and telemetry must be actively monitored (with appropriate dashboards and alarms).
- Systems must log all business events.
- Systems must log all security events with an audit trail.
- Understand and document the general system/solution flow and dependencies (from client to service provider(s) and all intermediaries).
- Identify and remediate problems by tracking incidents and performing root cause analysis.
