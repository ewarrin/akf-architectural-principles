# Portability

## Portability: Adaptable, Installable and Replaceable

- Strategically leverage the public cloud
- Choose cloud provider managed services when those services meet the non-functional requirements for a specific use case
- Align solution to public cloud and hosting strategy

## Manage Infrastructure as Code

- Autoscale to optimize for capacity and cost
- Automate infrastructure provisioning
- Prefer audit over tollgate review
- Ensure automated provisioning is repeatable and supports multiple regions when required by the business outcome
- Infrastructure as Code execution must be secure and auditable
