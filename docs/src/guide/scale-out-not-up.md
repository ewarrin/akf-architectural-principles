# Scale Out Not Up

Scale out by using smaller, cheaper servers to scale horizontally rather than bigger and bigger systems to scale.

Eventually you will get to a point where either the cost becomes uneconomical or there is no bigger hardware made.

## Benefits
**Increase Availability**
- Horizontally scaled systems have a reduced blast radius for failure – one device failing out of N impacts 1/N customers

**Increase Scalability**
- Horizontally scaled systems are easier and cheaper to grow, incremental change vs forklift upgrade
