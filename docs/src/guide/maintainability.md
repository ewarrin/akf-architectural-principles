# Maintainability

## Keep it simple, modular, and maintainable

- Complexity must be contained/encapsulated - interfaces must hide complexity, technology, and implementation details
- Build modular systems (services, libraries, or applications/products) rather than monoliths. Legacy monoliths should be decomposed when there is business value to do so
- Utilize design patterns such as ABC or MVC architecture to separate the concerns of display, business, and persistence logic
- There must be clear product domain boundaries and appropriate placement of function in order to aid with isolation
- Services must be cohesive but loosely coupled
- Data transformation logic belongs in message endpoints (e.g. Kafka consumers) and not within asynchronous communication "pipes" (e.g. Kafka)
- Service interfaces must conform to API style guide standards
- Use feature toggles to enable/disable functionality as a short-term solution to experimentation, A/B testing, or coordinating releases with other teams. Deprecate feature toggles once the functionality is considered stable, in order to reduce long-term system complexity. In practice, feature toggles should have a lifetime measured in days or weeks up to a couple of months
- Don't repeat yourself
