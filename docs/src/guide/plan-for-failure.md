# Plan For Failure

## Plan for failure but design for graceful failure

- Teams must analyze systems and sub-systems for failure modes
- Define and document customer-oriented Service Level Objectives (SLOs) in partnership with Product Management prior to designing a system
- Utilize instances, zones, and regions to provide required business continuity
- Isolate faults by avoiding microservice anti-patterns
- Isolate faults by separating customer traffic from other types of traffic such as internal reporting and public API requests
- Don't allow logical or physical single points of failure (SPOF), and aim for N+2 design - always have 3+ of anything
- Design to enable/disable resource-intensive functionality (safety valve toggles or kill switches) in order to alleviate system pressure or circumvent production problems. Examples include dynamically adjusting logging levels, disabling personalization for search results, etc. Kill switches generally have a long life span and may even exist for the life of the system
- Isolate code releases using deployment strategies that limit the exposure of risk to customers (e.g. blue-green, canary, etc.)
- Have contingency and/or rollback plans (for both software and data) in the event problems are discovered post-release
