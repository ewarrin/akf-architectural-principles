# Adopt Technology Thoughtfully

## Adopt technology thoughtfully and track what we use

- Choose technologies that align with talent available in the marketplace 
- Regularly track and manage our technology using the technology radar 
- Technical diversity will be managed to optimize talent, cost, complexity, and appropriateness
- Prefer mature technology over unproven technology (define mature, unproven)
- Immature technologies must only be used through controlled experimentation (see [Use Mature Technology](/guide/mature-technology.md))
- Choose solid, heavily adopted/supported, open source over closed source
- Choose open source that has commercial support available