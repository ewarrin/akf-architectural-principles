# The AKF Architect

- *Is not an ivory tower* -  Works shoulder to shoulder with the teams responsible for the delivery of the product 
- *Is an active listener*  - Understands the challenges faced by the product delivery teams
- *Is a champion* -  Helps the entire product and software delivery cycle
- *Is forward looking* - Steps back from the action to look forward and design the next phase of the product journey
- *Plans but is flexible* - Understands that nothing goes fully according to plan and that nothing is achieved without one
- *Creates the right environment* - Ensures the developers have an environment in which to successfully practice their craft
- *Ensures the technical alignment* – Gets the organization on the same page by constant measurement
