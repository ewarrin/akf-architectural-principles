# Isolate Faults
Keep failures from spreading by keeping things in their own swim lanes.

## Benefits
Swim lanes help with scalability and availability