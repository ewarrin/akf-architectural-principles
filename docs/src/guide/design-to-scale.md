# Design to Scale

- Each team is responsible for managing to a documented system capacity plan for their services. Teams must design systems for 20x to infinite scalability; implement and test for 3x to 20x scalability; but only deploy for 1.5x to 3x scalability. Any COTS or SaaS solutions we integrate must have a capacity plan defined prior to use; any scalability limitations must be documented as technical debt
- Any systems or sub-systems we build must be able to scale horizontally
- Application/service processes must be stateless
- Split dissimilar things as with functional decomposition
- Split data sets of similar things and clone the services supporting this data in order to isolate and scale on the z-axis (e.g. east coast customers served from different instances than west coast customers)
- Start and complete requests in the same datacenter or region
- Prefer asynchronous communication over synchronous communication
