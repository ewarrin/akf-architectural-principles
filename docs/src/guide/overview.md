# Overview

The principles found within this document form the foundation of Product-Driven Architecture. They establish the rules and guidelines that help us build for the future and provide highly available and performant products to delight our customers and associates. These principles are not recipes but provide the guardrails necessary to make functional and non-functional priorities meet our desired business outcomes. Principles should only be violated for sound business or technical reasons, and even then, these deviations from the principles will need to be approved by the Architecture Guild.  

This document does not describe topics in detail.  Over time, reference architectures, further documentation and, in some cases, example code will be supplied to help engineers address specific principles. 

## Intended Audience 

Every engineer, engineering manager and product manager involved in the delivery of a software product in support of a business outcome must have a broad understanding of the principles outlined in this document. Without that understanding, we risk making trade-offs that ultimately produce a solution that falls short of our goals. The Architecture Guild provides high level training for both product and senior engineering managers.  This document is intended for software engineers and engineering managers involved in making architectural decisions on a daily basis. 

## Goals
### Provide clear business value. 

- Collaborate with the business and product management to identify specific, measurable, attainable, relevant, and timely goals that align with the business strategy.  
- Continuously improve our processes, technology, and people while providing transparency. 
- Communicate with the business when decisions will incur technical debt.
- Product owners must define business value; teams must provide feasibility of solutions, potential debt, and expected CapEx, OpEx, and leave-behind costs. 
- Continually reevaluate architectures to reduce cost (cost should be considered as a variable to architectures) 
 
### Increase productivity

- New processes and technology must improve quality, developer productivity, time-to-market, or business value
- Architecture principles are intended to empower teams to self-govern, make their own decisions, but do so transparently. 
