# Design For Security, Privacy, and Compliance

Note that while security privacy and compliance are distinct fields of effort with distinct goals, they have a lot of overlap, and they are currently combined here

- Teams that own systems are responsible for ensuring they are secure and private
- Security and privacy policies and practices must be defined and published if they are to be enforced
- Security controls will be applied in proportion to business risk and cost. Security decisions must be documented for each system
- Systems must log security related events to an auditable repository
- Standard authentication systems must be used for both internal and external user authentication
- Systems must protect sensitive data in transit and at rest
- Production systems must have regular security testing
- Data must not become code
- Sensitive data must not appear in a URI
- Systems must classify and document themselves based on their data and activities (privacy classifications such as PII or SPI, regulatory classifications such as HIPAA or GLBAA, contractual classifications such as PCI)
- Production systems must reside in a vetted data center, cloud provider, or software as a service provider
- Production systems must have documented and followed release processes
- Production systems must use approved and documented encryption key management
- Software components must be retrieved from trusted repositories
- Retention rules should be documented and followed
- Systems with Personal Information must meet regulatory obligations for that data
