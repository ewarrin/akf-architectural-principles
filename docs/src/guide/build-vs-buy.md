# Buy Technology That is Non-Core

## Buy technology that is non-core, but build technology that is business differentiating 

**Regardless of how smart you and your team are – you simply aren’t the best at everything**

Your shareholders expect you to focus on the things that create competitive differentiation and shareholder value


## Summary of key points: 
- Developing solutions should be focused primarily on Strategic, business differentiating Capabilities/Commons  
- Buy vs. Build Selection Tree 
![Built vs Buy Tree](../assets/build-vs-buy-decision-tree.png)

## Key Things to Pay Attention to When Buying:
- Only buy technology that is viable, seamlessly integrates with the business, has commercial support, and aligns with the skill of the team.  
- Perform functional and non-functional evaluations and return on investment (ROI) projections prior to implementing any solution approach (Buy, SaaS, Build,...) 
- Open source should be considered a form of "Buy," but must be evaluated for all of the requirements the same as a Purchased/SaaS solution (Non-Functional Requirements are key) 
