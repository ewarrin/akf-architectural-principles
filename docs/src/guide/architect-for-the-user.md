# Architect For The User

## System architectures must be driven by desired user outcomes

- State the problem that you are trying to solve
- Gather user data to make sure the solution addresses a real-world problem
- Use the defined core design competencies
- Set concrete goals and define success criteria to prevent scope creep
